package b;

public class Car {
    private String color;
    public String model;
    protected int engine;

    public void start() {
        System.out.println("Engine is started");
    }

    public void stop() {
        System.out.println("Engine is stopped");
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getColor() {
        return color;
    }
}

class Mercedes extends Car {

    public Mercedes(String color, String model, int engine) {
        super.model = model;
        super.engine = engine;
        super.setColor(color);
    }

    public static void main(String[] args) {
        Mercedes a = new Mercedes("blue", "w213", 3000);
        a.start();
        a.printEngine();
        a.printModel();
        System.out.println(a.getColor());
    }

    public void printEngine() {
        System.out.println("Engine is " + super.engine);
    }

    public void printModel() {
        System.out.println("Model is "+ super.model);
    }
}

