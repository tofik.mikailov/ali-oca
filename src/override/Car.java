package override;

public class Car {
    public String model;

    public void startCar() {
        System.out.println(getModel() + " is started");
    }

    public final String getModel() {
        return "car";
    }
}

class Mercedes extends Car {


    public void startMercedes() {
        System.out.println(getModel() + " is started");
    }

    public static void main(String[] args) {
        Mercedes a = new Mercedes();
        a.startCar();
        a.startMercedes();
    }

}

